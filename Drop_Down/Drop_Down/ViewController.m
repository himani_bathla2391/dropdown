//
//  ViewController.m
//  Drop_Down
//
//  Created by Clicklabs 104 on 10/5/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation ViewController
NSArray *list;
NSArray *pic;
@synthesize button;
@synthesize label;
@synthesize table;

- (void)viewDidLoad {
    [super viewDidLoad];
    button.layer.cornerRadius=9;
    table.layer.cornerRadius=9;
    list=[[NSArray alloc]init];
    list= @[@"Chanel",@"Fendi",@"Gucci",@"Hermes",@"Lana Marks",@"Louis Vuitton",@"Prada",@"Puma"];
    pic=[[NSArray alloc]init];
    pic=@[@"chanelbag.jpg",@"fendibag.jpg",@"guccibag.jpeg",@"hermesbag.jpg",@"lana-marksbag.jpg",@"louis-vuitton.jpg",@"pradabag.jpg",@"pumapic.jpg"];
    table.hidden =YES;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)button:(id)sender {
    table.hidden=NO;
    
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView
                              dequeueReusableCellWithIdentifier:@"cellReuse"];
     cell.textLabel.text = list[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[pic objectAtIndex:indexPath.row]];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *messageAlert = [[UIAlertView alloc]initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [messageAlert show];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
